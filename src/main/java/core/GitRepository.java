package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {

	private String repoPath;

	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}
	
	public String getRepoPath() {
		return this.repoPath;
	}

	public String getHeadRef() {	
		String currentLine = "";
		String file = this.repoPath + "/HEAD";
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			currentLine = reader.readLine();
			currentLine = currentLine.substring(5);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return currentLine;

	}

	public String getRefHash(String headRef) {
		String currentLine = "";
		String file = this.repoPath + "/" + getHeadRef();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			currentLine = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return currentLine;

	}
}
