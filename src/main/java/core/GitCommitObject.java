package core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;

public class GitCommitObject extends GitObject {
	
	public GitCommitObject(String repoPath, String masterCommitHash) {
		super(repoPath, masterCommitHash);
	}

	public String getHash() {
		return super.getRefHash();
	}

	public String getTreeHash() {
		String file = super.getRepoPath() + "/objects/" + super.getRefHash().substring(0, 2) + "/"
				+ super.getRefHash().substring(2);

		try {
			InputStream input = new FileInputStream(new File(file));
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			InflaterInputStream decompresser = new InflaterInputStream(input);
			byte[] buffer = new byte[1];
			int len;

			while ((len = decompresser.read(buffer)) != -1)
				output.write(buffer, 0, len);
			return output.toString("UTF-8").split(" ")[2].substring(0, 40);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public String getParentHash() {
		String file = super.getRepoPath() + "/objects/" + super.getRefHash().substring(0, 2) + "/"
				+ super.getRefHash().substring(2);

		try {
			InputStream input = new FileInputStream(new File(file));
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			InflaterInputStream decompresser = new InflaterInputStream(input);
			byte[] buffer = new byte[1];
			int len;

			while ((len = decompresser.read(buffer)) != -1) {
				output.write(buffer, 0, len);
			}
			return output.toString("UTF-8").split(" ")[3].substring(0, 40);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public String getAuthor() {
		String file = super.getRepoPath() + "/objects/" + super.getRefHash().substring(0, 2) + "/"
				+ super.getRefHash().substring(2);

		try {
			InputStream input = new FileInputStream(new File(file));
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			InflaterInputStream decompresser = new InflaterInputStream(input);
			byte[] buffer = new byte[1];
			int len;

			while ((len = decompresser.read(buffer)) != -1) {
				output.write(buffer, 0, len);
			}
			return output.toString("UTF-8").split(" ")[4] + " " + output.toString("UTF-8").split(" ")[5] + " " + output.toString("UTF-8").split(" ")[6];
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

}
