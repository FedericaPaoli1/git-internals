package core;

public class GitObject {
	
	private GitRepository repo;
	private String refHash;
	private String type = "git object";
	private String size;
	
	public GitObject(String repoPath, String refHash) {
		this.repo = new GitRepository(repoPath);
		this.refHash = refHash;
	}
	
	public String getRepoPath() {
		return this.repo.getRepoPath();
	}
	
	public String getRefHash() {
		return this.refHash;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}


}
