package core;


public class GitBlobObject extends GitObject {

	private String content;

	public GitBlobObject(String repoPath, String refHash) {
		super(repoPath, refHash);
		setType("blob");
		this.content = "REPO DI PROVA\n=============\n\nSemplice repository Git di prova\n";
	}

	public String getContent() {
		return this.content;
	}

}
